﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleTester
{
    public class Review
    {
        //For some reason Xamarin requires use of properties in order to correctly bind the values to a listview
        private string title, content;

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                content = value;
            }
        }

        public Review(string title, string content)
        {
            this.Title = title;
            this.Content = content;
        }

        public override string ToString()
        {
            return string.Format("Review '{0}': {1}", Title, Content);
        }
    }
}
