﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SimpleTester
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Tmp1 : ContentPage
	{
		public Tmp1 ()
		{
			InitializeComponent ();
		}

        private bool areInputValid()
        {
            string header = reviewHeader.Text, content = reviewContent.Text;
            if (header == "" || content == "")
            {
                DisplayAlert("Review header or content null", "Either review content or header is null. Both are required", "Understood");
                return true;
            }
            else
            {
                return false;
            }
        }

        private void FieldTextChanged(object sender, TextChangedEventArgs e)
        {
            if(sender is Editor || sender is Entry)
            {
                changeFieldBackgrounds(sender);
            }
            else
            {
                DisplayAlert("Unknown editor", "Unknown field text change", "Close");
            }
        }

        private static void changeFieldBackgrounds(object sender)
        {
            Color emptyColor = Color.Red;
            Color validColor = Color.AntiqueWhite;

            if (sender is Editor)
            {
                Editor editor = (Editor)sender;

                if (editor.Text.Length == 0)
                {
                    editor.BackgroundColor = emptyColor;
                }
                else
                {
                    editor.BackgroundColor = validColor;
                }
            }
            else if(sender is Entry)
            {
                Entry entry = (Entry)sender;

                if (entry.Text.Length == 0)
                {
                    entry.BackgroundColor = emptyColor;
                }
                else
                {
                    entry.BackgroundColor = validColor;
                }
            }
        }

        private void Submit_Clicked(object sender, EventArgs e)
        {
            //are all required fields filled in?
            if (areInputValid())
            {
                //Submit and to main menu
               
            }
            else
            {
                changeFieldBackgrounds(reviewHeader);
                changeFieldBackgrounds(reviewContent);
            }
        }

        

        private async void Cancel_Clicked(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Confirm exit", "Are you sure you want to exit? Unsent data will be lost.", "Yes", "No");

            if(answer)
            {
                //Return to main menu
                await Navigation.PopAsync();
            }
        }

       
    }
}