﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace SimpleTester
{
    class WebServicer
    {

        public static async Task<JObject> GetJson(string source)
        {
            HttpClient httpClient = new HttpClient();
            HttpResponseMessage msg = await httpClient.GetAsync(source);

            if (msg.IsSuccessStatusCode)
            {
                string results = await msg.Content.ReadAsStringAsync();
                JObject obj = JObject.Parse(results);
                return obj;
            }
            else
            {
                return null;
            }
        }

    }
}
