﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.ObjectModel;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace SimpleTester
{
	public partial class MainPage : ContentPage
	{
        public ObservableCollection<Review> shownReviews = new ObservableCollection<Review>();
        
        public string ReviewSource = "http://student.labranet.jamk.fi/~K1967/Xamarin/information.json";


        public MainPage()
        {
            InitializeComponent();
            PopulateListview();
            ReviewList.ItemsSource = shownReviews;
        }

        private async void PopulateListview()
        {
            JObject results = await WebServicer.GetJson(ReviewSource);
            
            if(results != null)
            {
                JToken token = results.GetValue("ReviewList");
                JArray array = (JArray)token;

                foreach(JObject obj in array)
                {
                    string name = (string)obj.GetValue("Title"), 
                        content = (string)obj.GetValue("Content");
                    shownReviews.Add(new Review(name, content));
                }
            }
        }

        private void PopupButton_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("Hello", "Popup notification seems to work nicely", "Correct");
        }

        private void OpenReview_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Tmp1());
        }
    }
}
