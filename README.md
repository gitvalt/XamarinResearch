# Xamarin test application

## Introduction
This is a simple Xamarin application designed to test basic functions of Xamarin. Tested such features as Popup's, Dialogs, Fetching data from http, binding data to a listview.

## Testing
Mainly tested with Android emulator / device. Iphone and Windows phone functionality unknown

## Developer
Valtteri Seuranen
